// Copyright 2022 the GLanguage authors. All rights reserved. MIT license.
#![feature(once_cell)]

use clap::{crate_version, Parser as ClapParser, Subcommand};
use glang_core::interpreter::Interpreter;
use glang_core::lexer::Lexer;
use glang_core::parser::Parser;
use glang_core::primitive::Type;
use rustyline::error::ReadlineError;
use rustyline::{Config, EditMode, Editor};

#[derive(Debug, ClapParser)]
#[clap(version, about, long_about = None)]
struct Cli {
	#[clap(subcommand)]
	command: Option<Commands>,
}

#[derive(Debug, Subcommand)]
enum Commands {
	/// Read Eval Print Loop
	Repl {
		#[clap(last = true)]
		slop: Vec<String>,
	},
	/// Eval script
	Eval {
		code: String,
		#[clap(short, long)]
		inspect: bool,
		#[clap(last = true)]
		slop: Vec<String>,
	},
}

fn main() {
	let args: Cli = Cli::parse();

	let command: Commands = match args.command {
		Some(c) => c,
		None => Commands::Repl { slop: Vec::new() },
	};

	match command {
		Commands::Repl { .. } => repl(),
		Commands::Eval { code, inspect, .. } => {
			let lexer: Lexer = Lexer::new(code);
			let mut parser: Parser = Parser::new(lexer);
			let mut interpreter: Interpreter = Interpreter::new();
			match parser.parse() {
				Ok(ast) => match interpreter.eval(ast) {
					Ok(_) => {}
					Err(err) => {
						eprintln!("{}", err)
					}
				},
				Err(err) => {
					eprintln!("{}", err)
				}
			};

			if inspect {
				repl()
			}
		}
	}
}

fn repl() {
	let config: Config = Config::builder()
		.history_ignore_space(true)
		.edit_mode(EditMode::Vi)
		.auto_add_history(true)
		.tab_stop(4)
		.indent_size(4)
		.build();
	let mut rl: Editor<()> = Editor::<()>::with_config(config);
	rl.load_history(".gl_history").unwrap_or(());

	let mut rl_line: usize = 0;
	println!("GL {}", crate_version!());
	println!("exit using ctrl+d or exit()");
	loop {
		let prompt: String = format!("[{}]-> ", rl_line);
		rl_line += 1;

		match rl.readline(&prompt) {
			Ok(code) => {
				let lexer: Lexer = Lexer::new(code);
				let mut parser: Parser = Parser::new(lexer);
				let mut interpreter: Interpreter = Interpreter::new();
				match parser.parse() {
					Ok(ast) => match interpreter.eval(ast) {
						Ok(r) => match r {
							Type::Str(string) => {
								println!("\"{}\"", string)
							}
							t => {
								println!("{}", t)
							}
						},
						Err(err) => {
							eprintln!("{}", err)
						}
					},
					Err(err) => {
						eprintln!("{}", err)
					}
				};
			}
			Err(err) => match err {
				ReadlineError::Eof => break,
				ReadlineError::Interrupted => {
					println!("exit using ctrl+d or exit()");
					continue;
				}
				_ => break eprintln!("Error: {}", err),
			},
		}
	}

	rl.save_history(".gl_history").unwrap_or(());
}
