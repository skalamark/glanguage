// Copyright 2022 the GLanguage authors. All rights reserved. MIT license.

use glang_core::ast::AbstractSyntaxTree;
use glang_core::interpreter::Interpreter;
use glang_core::lexer::Lexer;
use glang_core::parser::Parser;

fn parse(source_code: &str) -> Option<AbstractSyntaxTree> {
	let lexer: Lexer = Lexer::new(source_code.to_string());
	let mut parser: Parser = Parser::new(lexer);
	match parser.parse() {
		Ok(ast) => Some(ast),
		Err(err) => {
			eprintln!("{}", err);
			None
		}
	}
}

#[test]
fn whitespace() {
	let ast: AbstractSyntaxTree = parse(" ").unwrap();
	assert_eq!(ast.len(), 0);
	let mut interpreter = Interpreter::new();
	println!("{:?}", interpreter.eval(ast));
}

#[test]
fn integer() {
	let ast: AbstractSyntaxTree = parse(" 0; 1; 2; 3; 4; 5; 6; 7; 8; 9; ").unwrap();
	assert_eq!(ast.len(), 10);
	let mut interpreter = Interpreter::new();
	println!("{:?}", interpreter.eval(ast));
}

#[test]
fn string() {
	let ast: AbstractSyntaxTree = parse("\" a b c x y z \"").unwrap();
	assert_eq!(ast.len(), 1);
	let mut interpreter = Interpreter::new();
	println!("{:?}", interpreter.eval(ast));
}
