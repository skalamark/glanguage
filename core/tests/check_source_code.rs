// Copyright 2022 the GLanguage authors. All rights reserved. MIT license.

use glang_core::lexer::Lexer;

fn check_source_code(source_code: &str) -> bool {
	let mut lexer: Lexer = Lexer::new(source_code.to_string());
	match lexer.check_source_code() {
		Ok(_) => true,
		Err(err) => {
			eprintln!("{}", err);
			false
		}
	}
}

#[test]
fn whitespace() {
	assert!(check_source_code(" "));
}

#[test]
fn integer() {
	assert!(check_source_code(" 0 1 2 3 4 5 6 7 8 9 "));
}

#[test]
fn string() {
	assert!(check_source_code("\" a b c x y z \""));
}
