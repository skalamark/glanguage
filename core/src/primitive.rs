// Copyright 2022 the GLanguage authors. All rights reserved. MIT license.

use crate::token::{Kind, Token};
use std::fmt::{Display, Formatter};

#[derive(Clone, Debug)]
pub enum Type {
	Null,
	Int(isize),
	Str(String),
}

impl From<Token> for Type {
	fn from(token: Token) -> Self {
		match token.kind {
			Kind::Integer => Type::Int(token.value.parse().expect("invalid integer value")),
			Kind::String => Type::Str(token.value),
			_ => Type::Null,
		}
	}
}

impl Display for Type {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		match self {
			Type::Null => {
				write!(f, "null")
			}
			Type::Int(integer) => {
				write!(f, "{}", integer)
			}
			Type::Str(string) => {
				write!(f, "{}", string)
			}
		}
	}
}
