// Copyright 2022 the GLanguage authors. All rights reserved. MIT license.

#[derive(Clone, Debug, PartialEq)]
pub enum Kind {
	EOF,        // \0
	NewLine,    // \r | \n
	WhiteSpace, // ' ' | \t
	SemiColon,  // ;
	Integer,    // U+0030 '0' ..= U+0039 '9'
	String,     // "
}

impl Kind {
	pub fn classify(character: Option<char>) -> Result<Kind, String> {
		match character {
			None => Ok(Kind::EOF),
			Some(c) => match c {
				'\r' | '\n' => Ok(Kind::NewLine),
				' ' | '\t' => Ok(Kind::WhiteSpace),
				';' => Ok(Kind::SemiColon),
				c if c.is_ascii_digit() => Ok(Kind::Integer),
				c if c.is_ascii_punctuation() => match c {
					'"' => Ok(Kind::String),
					_ => Err(format!("invalid character '{}'", c)),
				},
				_ => Err(format!("invalid character '{}'", c)),
			},
		}
	}
}

#[derive(Clone, Debug)]
pub struct Token {
	pub kind: Kind,
	pub value: String,
}

impl Token {
	pub fn new(kind: Kind, value: String) -> Self {
		Self { kind, value }
	}
}
