// Copyright 2022 the GLanguage authors. All rights reserved. MIT license.

use crate::ast::{AbstractSyntaxTree, Node, Operation};
use crate::primitive::Type;

pub struct Interpreter {}

impl Interpreter {
	pub fn new() -> Self {
		Self {}
	}

	fn node(&mut self, node: Node) -> Result<Type, String> {
		match *node.get_operation() {
			Operation::Empty => Ok(Type::Null),
			Operation::Constant(c) => Ok(c),
		}
	}

	pub fn eval(&mut self, ast: AbstractSyntaxTree) -> Result<Type, String> {
		let mut result: Type = Type::Null;
		for node in ast {
			result = self.node(node)?;
		}
		Ok(result)
	}
}
