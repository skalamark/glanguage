// Copyright 2022 the GLanguage authors. All rights reserved. MIT license.

use crate::primitive::Type;
use crate::token::Token;

#[derive(Clone, Debug)]
pub struct AbstractSyntaxTree {
	nodes: Vec<Node>,
	position: usize,
}

impl AbstractSyntaxTree {
	pub fn new() -> Self {
		Self {
			nodes: Vec::new(),
			position: 0,
		}
	}

	pub fn push(&mut self, node: Node) {
		self.nodes.push(node)
	}

	pub fn len(&self) -> usize {
		self.nodes.len()
	}

	pub fn next(&mut self) -> Option<Node> {
		match self.nodes.get(self.position) {
			None => None,
			Some(n) => {
				self.position += 1;
				Some(n.clone())
			}
		}
	}
}

impl Iterator for AbstractSyntaxTree {
	type Item = Node;

	fn next(&mut self) -> Option<Self::Item> {
		self.next()
	}
}

#[derive(Clone, Debug)]
pub struct Node {
	operation: Box<Operation>,
	value: String,
}

impl Node {
	pub fn new(operation: Operation, value: String) -> Self {
		Self {
			operation: Box::new(operation),
			value,
		}
	}

	pub fn get_operation(&self) -> Box<Operation> {
		self.operation.clone()
	}

    pub fn get_value(&self) -> String {
        self.value.clone()
    }

	pub fn t_constant(token: Token) -> Self {
		Self::new(Operation::Constant(Type::from(token.clone())), token.value)
	}
}

#[derive(Clone, Debug)]
pub enum Operation {
	Empty,
	Constant(Type),
}
