// Copyright 2022 the GLanguage authors. All rights reserved. MIT license.

use crate::ast::{AbstractSyntaxTree, Node};
use crate::lexer::Lexer;
use crate::token::{Kind, Token};

pub struct Parser {
	lexer: Lexer,
	cache_tokens: Vec<Token>,
	errors: Vec<String>,
}

impl Parser {
	pub fn new(lexer: Lexer) -> Self {
		Self {
			lexer,
			cache_tokens: Vec::new(),
			errors: Vec::new(),
		}
	}

	fn next_token(&mut self) -> Option<Token> {
		if self.cache_tokens.len() > 0 {
			Some(self.cache_tokens.remove(0))
		} else {
			match self.lexer.consume() {
				Ok(nt) => nt,
				Err(err) => {
					self.errors.push(err);
					None
				}
			}
		}
	}

	fn next_token_while(&mut self, kind: Kind) -> Option<Token> {
		if let Some(t) = self.next_token() {
			if t.kind == kind {
				Some(t)
			} else {
				self.cache_tokens.push(t);
				None
			}
		} else {
			None
		}
	}

	fn statement(&mut self) -> Option<Node> {
		let t = match self.next_token() {
			None => return None,
			Some(t) => t,
		};

		let r: Option<Node> = match t.kind {
			Kind::EOF => None,
			Kind::WhiteSpace => return self.statement(),
			Kind::Integer => {
				let mut token_integer: Token = t.clone();
				while let Some(nt) = self.next_token_while(t.kind.clone()) {
					token_integer.value.push_str(&nt.value);
				}
				Some(Node::t_constant(token_integer))
			}
			Kind::String => {
				let mut token_string: Token = t.clone();
				while let Some(nt) = self.next_token_while(t.kind.clone()) {
					token_string.value.push_str(&nt.value);
				}
				Some(Node::t_constant(token_string))
			}
			_ => {
				self.errors.push(format!("invalid syntax '{}'", t.value));
				None
			}
		};

		if r.is_some() {
			let mut ok: bool = false;
			loop {
				let next_token: Token = match self.next_token() {
					None => break r,
					Some(nt) => nt,
				};
				match next_token.kind {
					Kind::EOF => break r,
					Kind::WhiteSpace => {}
					Kind::NewLine | Kind::SemiColon if ok == false => {
						ok = true;
					}
					_ => {
						if ok {
							self.cache_tokens.push(next_token);
							break r;
						}
						self.errors.push(format!(
							"expected ';', newline or eof found '{}' after '{}'",
							next_token.value, t.value
						));
						break None;
					}
				}
			}
		} else {
			None
		}
	}

	pub fn parse(&mut self) -> Result<AbstractSyntaxTree, String> {
		let mut ast: AbstractSyntaxTree = AbstractSyntaxTree::new();
		while let Some(n) = self.statement() {
			if self.errors.len() > 0 {
				return Err(format!("{}", self.errors.first().unwrap()));
			}
			ast.push(n)
		}
		if self.errors.len() > 0 {
			return Err(format!("{}", self.errors.first().unwrap()));
		}
		Ok(ast)
	}
}
